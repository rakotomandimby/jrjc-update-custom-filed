package com.mihamina.util;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.BasicProject;
import com.atlassian.jira.rest.client.api.domain.BasicWatchers;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.FieldInput;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.atlassian.jira.rest.client.internal.ServerVersionConstants;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.google.common.collect.Lists;
import org.codehaus.jettison.json.JSONException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

//import com.mihamina.util.Ticket;


/**
 * A sample code how to use JRJC library
 *
 * @since v0.1
 */
public class App {

    private static URI jiraServerUri = URI.create("https://rakotomandimby.atlassian.net/");
    private static String username ;
    private static String password ;
    private static boolean quiet = false;
    public static void main(String[] args) throws URISyntaxException, JSONException, IOException {
        parseArgs(args);

        final AsynchronousJiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        final JiraRestClient restClient = factory.createWithBasicHttpAuthentication(jiraServerUri, username, password);
        try {
            final int buildNumber = restClient.getMetadataClient().getServerInfo().claim().getBuildNumber();
            final Issue issue = restClient.getIssueClient().getIssue("CAISSE-201").claim();
            println(issue);
            // IssueField{id=customfield_10400, name=Ariary, type=null, value=400000.0}
        }
        finally {
            restClient.close();
        }
    }

    private static void println(Object o) {
        if (!quiet) {
            System.out.println(o);
        }
    }

    private static void parseArgs(String[] argsArray) throws URISyntaxException {
        final List<String> args = Lists.newArrayList(argsArray);
        if (args.contains("-q")) {
            quiet = true;
            args.remove(args.indexOf("-q"));
        }
    
        if (!args.isEmpty()) {
            username = args.get(0);
            password = args.get(1);
        }
    }

}
